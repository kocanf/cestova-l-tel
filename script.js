var countryInfo = {
    'al': 'Albánie je země v jihovýchodní Evropě. Hlavní město je Tirana.',
    'ad': 'Andorra je země v jihozápadní Evropě. Hlavní město je Andorra la Vella.',
    'am': 'Arménie je země v kavkazské oblasti Eurasie. Hlavní město je Jerevan.',
    'at': 'Rakousko je země ve střední Evropě. Hlavní město je Vídeň.',
    'az': 'Ázerbájdžán je země v kavkazské oblasti Eurasie. Hlavní město je Baku.',
    'by': 'Bělorusko je země ve východní Evropě. Hlavní město je Minsk.',
    'be': 'Belgie je země v západní Evropě. Hlavní město je Brusel.',
    'ba': 'Bosna a Hercegovina je země v jihovýchodní Evropě. Hlavní město je Sarajevo.',
    'bg': 'Bulharsko je země v jihovýchodní Evropě. Hlavní město je Sofie.',
    'hr': 'Chorvatsko je země ve střední Evropě. Hlavní město je Záhřeb.',
    'cy': 'Kypr je země ve východním Středomoří. Hlavní město je Nikósie.',
    'cz': 'Česko je země ve střední Evropě. Hlavní město je Praha.',
    'dk': 'Dánsko je země v severní Evropě. Hlavní město je Kodaň.',
    'ee': 'Estonsko je země v severní Evropě. Hlavní město je Tallinn.',
    'fi': 'Finsko je země v severní Evropě. Hlavní město je Helsinky.',
    'fr': 'Francie je země v západní Evropě. Hlavní město je Paříž.',
    'ge': 'Gruzie je země v kavkazské oblasti Eurasie. Hlavní město je Tbilisi.',
    'de': 'Německo je země ve střední Evropě. Hlavní město je Berlín.',
    'gr': 'Řecko je země v jihovýchodní Evropě. Hlavní město je Atény.',
    'hu': 'Maďarsko je země ve střední Evropě. Hlavní město je Budapešť.',
    'is': 'Island je země v severní Evropě. Hlavní město je Reykjavík.',
    'ie': 'Irsko je země v severozápadní Evropě. Hlavní město je Dublin.',
    'it': 'Itálie je země v jižní Evropě. Hlavní město je Řím.',
    'kz': 'Kazachstán je země v centrální Asii a částečně ve východní Evropě. Hlavní město je Astana.',
    'lv': 'Lotyšsko je země v severní Evropě. Hlavní město je Riga.',
    'li': 'Lichtenštejnsko je země ve střední Evropě. Hlavní město je Vaduz.',
    'lt': 'Litva je země v severní Evropě. Hlavní město je Vilnius.',
    'lu': 'Lucembursko je země v západní Evropě. Hlavní město je Lucemburk.',
    'mt': 'Malta je země v jižní Evropě. Hlavní město je Valletta.',
    'md': 'Moldavsko je země ve východní Evropě. Hlavní město je Kišiněv.',
    'mc': 'Monako je země v západní Evropě. Hlavní město je Monako.',
    'me': 'Černá Hora je země v jihovýchodní Evropě. Hlavní město je Podgorica.',
    'nl': 'Nizozemsko je země v západní Evropě. Hlavní město je Amsterdam.',
    'no': 'Norsko je země v severní Evropě. Hlavní město je Oslo.',
    'pl': 'Polsko je země ve střední Evropě. Hlavní město je Varšava.',
    'pt': 'Portugalsko je země v jihozápadní Evropě. Hlavní město je Lisabon.',
    'ro': 'Rumunsko je země ve východní Evropě. Hlavní město je Bukurešť.',
    'ru': 'Rusko je země ve východní Evropě a severní Asii. Hlavní město je Moskva.',
    'sm': 'San Marino je země v jižní Evropě. Hlavní město je San Marino.',
    'rs': 'Srbsko je země v jihovýchodní Evropě. Hlavní město je Bělehrad.',
    'sk': 'Slovensko je země ve střední Evropě. Hlavní město je Bratislava.',
    'si': 'Slovinsko je země ve střední Evropě. Hlavní město je Lublaň.',
    'es': 'Španělsko je země v jihozápadní Evropě. Hlavní město je Madrid.',
    'se': 'Švédsko je země v severní Evropě. Hlavní město je Stockholm.',
    'ch': 'Švýcarsko je země ve střední Evropě. Hlavní město je Bern.',
    'ua': 'Ukrajina je země ve východní Evropě. Hlavní město je Kyjev.',
    'gb': 'Spojené království je země v severozápadní Evropě. Hlavní město je Londýn.',
    'va': 'Vatikán je městský stát v jižní Evropě. Hlavní město je Vatikán.'    
};
var wantToVisit = {
    'cz': 'Navštívím Prahu, Plzeň, Cheb a České Budějovice',
    'sk': 'Navštívím Bratislavu, Vysoké Tatry, Piešťany a Košice',
    'de': 'Navštívím Berlín, Mnichov, Frankfurt a Hamburg',
    'fr': 'Navštívím Paříž, Lyon, Marseille a Bordeaux',
    'it': 'Navštívím Řím, Milán, Benátky a Florencii',
    'es': 'Navštívím Madrid, Barcelonu, Sevillu a Valenci',
    'uk': 'Navštívím Londýn, Manchester, Edinburgh a Bristol',
    'at': 'Navštívím Vídeň, Salzburg, Innsbruck a Graz',
    'be': 'Navštívím Brusel, Antverpy, Gent a Bruggy',
    'nl': 'Navštívím Amsterdam, Rotterdam, Haag a Utrecht',
    'pl': 'Navštívím Varšavu, Krakov, Gdaňsk a Poznaň',
    'se': 'Navštívím Stockholm, Göteborg, Malmö a Uppsala',
    'no': 'Navštívím Oslo, Bergen, Trondheim a Stavanger',
    'fi': 'Navštívím Helsinky, Turku, Tampere a Oulu',
    'dk': 'Navštívím Kodaň, Aarhus, Odense a Aalborg',
    'ch': 'Navštívím Curych, Ženevu, Basilej a Bern',
    'pt': 'Navštívím Lisabon, Porto, Faro a Coimbru',
    'gr': 'Navštívím Athény, Soluň, Patras a Heraklion',
    'ie': 'Navštívím Dublin, Cork, Galway a Limerick',
    'hu': 'Navštívím Budapešť, Debrecín, Szeged a Miskolc',
    'ro': 'Navštívím Bukurešť, Cluj-Napoca, Temešvár a Iași',
    'bg': 'Navštívím Sofii, Plovdiv, Varnu a Burgas',
    'hr': 'Navštívím Záhřeb, Split, Rijeku a Dubrovník',
    'si': 'Navštívím Lublaň, Maribor, Celje a Koper',
    'lv': 'Navštívím Rigu, Daugavpils, Liepāju a Jelgavu',
    'lt': 'Navštívím Vilnius, Kaunas, Klaipėdu a Šiauliai',
    'ee': 'Navštívím Tallinn, Tartu, Narvu a Pärnu',
    'by': 'Navštívím Minsk, Brest, Hrodnu a Vitebsk',
    'ua': 'Navštívím Kyjev, Lvov, Oděsu a Charkov',
    'md': 'Navštívím Kišiněv, Tiraspol, Bălți a Bender',
    'al': 'Navštívím Tiranu, Drač, Vloru a Shkodër',
    'mk': 'Navštívím Skopje, Ohrid, Bitolu a Tetovo',
    'ba': 'Navštívím Sarajevo, Banja Luku, Mostar a Tuzlu',
    'me': 'Navštívím Podgoricu, Kotor, Budvu a Herceg Novi',
    'rs': 'Navštívím Bělehrad, Novi Sad, Niš a Kragujevac',
    'mt': 'Navštívím Valletta, Sliemu, St. Julian\'s a Mdinu',
    'is': 'Navštívím Reykjavik, Akureyri, Keflavík a Selfoss',
    'lu': 'Navštívím Lucemburk, Esch-sur-Alzette, Differdange a Dudelange',
    'cy': 'Navštívím Nikósii, Limassol, Larnaku a Pafos',
    'li': 'Navštívím Vaduz, Schaan, Balzers a Triesen',
    'mc': 'Navštívím Monako, Monte Carlo, La Condamine a Fontvieille',
    'sm': 'Navštívím San Marino, Borgo Maggiore, Serravalle a Domagnano',
    'ad': 'Navštívím Andorru la Vella, Escaldes-Engordany, Encamp a Sant Julià de Lòria',
    'va': 'Navštívím Vatikánský městský stát',
    'ru': 'Navštívím Moskvu, Petrohrad, Kazaň a Soči'
};
var visitedCountry = {
    'cz': 'Navštívil jsem Prahu, Plzeň, Cheb a České Budějovice',
    'de': 'Navštívil jsem Berlín, Mnichov, Frankfurt a Hamburg',
    'fr': 'Navštívil jsem Paříž, Lyon, Marseille a Bordeaux',
    'it': 'Navštívil jsem Řím, Milán, Benátky a Florencii',
    'es': 'Navštívil jsem Madrid, Barcelonu, Sevillu a Valenci',
    'uk': 'Navštívil jsem Londýn, Manchester, Edinburgh a Bristol',
    'at': 'Navštívil jsem Vídeň, Salzburg, Innsbruck a Graz',
    'be': 'Navštívil jsem Brusel, Antverpy, Gent a Bruggy',
    'nl': 'Navštívil jsem Amsterdam, Rotterdam, Haag a Utrecht',
    'pl': 'Navštívil jsem Varšavu, Krakov, Gdaňsk a Poznaň',
    'se': 'Navštívil jsem Stockholm, Göteborg, Malmö a Uppsala',
    'no': 'Navštívil jsem Oslo, Bergen, Trondheim a Stavanger',
    'fi': 'Navštívil jsem Helsinky, Turku, Tampere a Oulu',
    'dk': 'Navštívil jsem Kodaň, Aarhus, Odense a Aalborg',
    'ch': 'Navštívil jsem Curych, Ženevu, Basilej a Bern',
    'pt': 'Navštívil jsem Lisabon, Porto, Faro a Coimbra',
    'gr': 'Navštívil jsem Athény, Soluň, Patras a Heraklion',
    'ie': 'Navštívil jsem Dublin, Cork, Galway a Limerick',
    'hu': 'Navštívil jsem Budapešť, Debrecín, Szeged a Miskolc',
    'ro': 'Navštívil jsem Bukurešť, Cluj-Napoca, Temešvár a Iași',
    'bg': 'Navštívil jsem Sofii, Plovdiv, Varna a Burgas',
    'hr': 'Navštívil jsem Záhřeb, Split, Rijeku a Dubrovník',
    'si': 'Navštívil jsem Lublaň, Maribor, Celje a Koper',
    'sk': 'Navštívil jsem Bratislavu, Košice, Prešov a Žilinu',
    'lv': 'Navštívil jsem Rigu, Daugavpils, Liepāju a Jelgavu',
    'lt': 'Navštívil jsem Vilnius, Kaunas, Klaipėdu a Šiauliai',
    'ee': 'Navštívil jsem Tallinn, Tartu, Narvu a Pärnu',
    'by': 'Navštívil jsem Minsk, Brest, Hrodnu a Vitebsk',
    'ua': 'Navštívil jsem Kyjev, Lvov, Oděsu a Charkov',
    'md': 'Navštívil jsem Kišiněv, Tiraspol, Bălți a Bender',
    'al': 'Navštívil jsem Tiranu, Drač, Vloru a Shkodër',
    'mk': 'Navštívil jsem Skopje, Ohrid, Bitolu a Tetovo',
    'ba': 'Navštívil jsem Sarajevo, Banja Luku, Mostar a Tuzlu',
    'me': 'Navštívil jsem Podgoricu, Kotor, Budvu a Herceg Novi',
    'rs': 'Navštívil jsem Bělehrad, Novi Sad, Niš a Kragujevac',
    'mt': 'Navštívil jsem Valletta, Sliema, St. Julians a Mdinu',
    'is': 'Navštívil jsem Reykjavik, Akureyri, Keflavík a Selfoss',
    'lu': 'Navštívil jsem Lucemburk, Esch-sur-Alzette, Differdange a Dudelange',
    'cy': 'Navštívil jsem Nikósii, Limassol, Larnaku a Pafos',
    'li': 'Navštívil jsem Vaduz, Schaan, Balzers a Triesen'
};
var countryNames = {
    'cz': 'Česká republika',
    'sk': 'Slovenská republika',
    'de': 'Spolková republika Německo',
    'fr': 'Francouzská republika',
    'it': 'Italská republika',
    'es': 'Španělské království',
    'uk': 'Spojené království Velké Británie a Severního Irska',
    'at': 'Rakouská republika',
    'be': 'Belgické království',
    'nl': 'Nizozemské království',
    'pl': 'Polská republika',
    'se': 'Švédské království',
    'no': 'Norské království',
    'fi': 'Finská republika',
    'dk': 'Dánské království',
    'ch': 'Švýcarská konfederace',
    'pt': 'Portugalská republika',
    'gr': 'Helénská republika',
    'ie': 'Irská republika',
    'hu': 'Maďarská republika',
    'ro': 'Rumunsko',
    'bg': 'Bulharská republika',
    'hr': 'Chorvatská republika',
    'si': 'Slovinská republika',
    'lv': 'Lotyšská republika',
    'lt': 'Litevská republika',
    'ee': 'Estonská republika',
    'by': 'Běloruská republika',
    'ua': 'Ukrajina',
    'md': 'Moldavská republika',
    'al': 'Albánská republika',
    'mk': 'Republika Severní Makedonie',
    'ba': 'Bosna a Hercegovina',
    'me': 'Černohorská republika',
    'rs': 'Srbská republika',
    'mt': 'Maltská republika',
    'is': 'Islandská republika',
    'lu': 'Lucemburské velkovévodství',
    'cy': 'Kyperská republika',
    'li': 'Lichtenštejnské knížectví',
    'mc': 'Monacké knížectví',
    'sm': 'Sanmarinská republika',
    'ad': 'Andorrské knížectví',
    'va': 'Vatikánský městský stát',
    'ru': 'Ruská federace'
};

var CountryPOI = {
    'cz': 'V České republice je toho spousty k vidění, ale základem je rozhodně stará Praha, kolonáda lázeňských měst <a div="cztext" style="color:white;text-decoration:none;" href="https://www.karlovyvary.cz/cs/10-duvodu-proc-navstivit-karlovy-vary" target="_blank">Karlovy Vary</a>, a nebo si zajít na točené pivo do <a id="cz" div="cztext" style="color:white;text-decoration:none;"href=#>Českých Budějovic</a> či <a href=# div="cztext" style="color:white;text-decoration:none;" id="cz">Plzně</a>.',
    'sk': 'Na Slovensku stojí za návštěvu Bratislava, nádherná Vysoké Tatry, a termální lázně v Piešťanech.',
    'de': 'V Německu byste neměli minout Berlín s jeho historií, romantické bavorské zámky jako je <a href="https://www.neuschwanstein.de/englisch/tourist/" target="_blank" style="color:black;text-decoration:none;">Neuschwanstein</a>, a Oktoberfest v Mnichově.',
    'fr': 'Ve Francii navštivte Paříž s Eiffelovou věží, krásné pobřeží na Azurovém pobřeží, a historické zámky v údolí Loiry.',
    'it': 'V Itálii nezapomeňte na Řím s Koloseem, Benátky s kanály, a Florencii s uměním renesance.',
    'es': 'Ve Španělsku navštivte Barcelonu s architekturou Gaudího, Madrid s jeho muzei, a krásné pláže na Costa del Sol.',
    'uk': 'Ve Spojeném království navštivte Londýn s jeho památkami, historické univerzitní město Oxford, a krásnou přírodu v Lake District.',
    'at': 'V Rakousku navštivte Vídeň s jejími paláci, Salzburg jako rodiště Mozarta, a krásné alpské oblasti jako Tyrolsko.',
    'be': 'V Belgii navštivte Brusel s jeho Grand Place, historické město Bruggy, a Antverpy s jeho uměním.',
    'nl': 'V Nizozemsku navštivte Amsterdam s jeho kanály, květinové zahrady v Keukenhofu, a historické větrné mlýny v Kinderdijku.',
    'pl': 'V Polsku navštivte Krakov s jeho starým městem, Varšavu s moderní architekturou, a přírodní krásy Tatranského národního parku.',
    'se': 'Ve Švédsku navštivte Stockholm s jeho ostrovy, Göteborg s přístavem, a krásné národní parky jako Abisko.',
    'no': 'V Norsku navštivte Oslo s jeho muzei, fjordy jako Geirangerfjord, a polární záři v Tromsø.',
    'fi': 'Ve Finsku navštivte Helsinky s moderní architekturou, přírodu v Laponsku, a mnoho jezer a lesů.',
    'dk': 'V Dánsku navštivte Kodaň s Malou mořskou vílou, historické město Aarhus, a krásné pláže na Severním moři.',
    'ch': 'Ve Švýcarsku navštivte Curych s jeho starým městem, Ženevu s jezery, a alpská střediska jako Zermatt.',
    'pt': 'V Portugalsku navštivte Lisabon s jeho kopci, Porto s jeho víny, a krásné pláže na Algarve.',
    'gr': 'V Řecku navštivte Athény s Akropolí, ostrovy jako Santorini, a kláštery Meteora.',
    'ie': 'V Irsku navštivte Dublin s jeho puby, Cliffs of Moher, a nádhernou krajinu v národním parku Killarney.',
    'hu': 'V Maďarsku navštivte Budapešť s jejím lázeňstvím, jezero Balaton, a historické město Eger.',
    'ro': 'V Rumunsku navštivte Bukurešť s jejími paláci, Transylvánii s hradem Bran, a krásné Karpaty.',
    'bg': 'V Bulharsku navštivte Sofii s jejími kostely, Plovdiv s antickými památkami, a pobřeží Černého moře.',
    'hr': 'V Chorvatsku navštivte Dubrovník s jeho hradbami, Split s Diokleciánovým palácem, a nádherné ostrovy jako Hvar.',
    'si': 'Ve Slovinsku navštivte Lublaň s jejími mosty, Bledské jezero, a krásné jeskyně Postojna.',
    'lv': 'V Lotyšsku navštivte Rigu s jejím starým městem, národní park Gauja, a krásné pobřeží.',
    'lt': 'V Litvě navštivte Vilnius s jeho barokní architekturou, Kaunas s historickými památkami, a národní park Aukštaitija.',
    'ee': 'V Estonsku navštivte Tallinn s jeho středověkým centrem, Tartu s univerzitou, a přírodní parky jako Lahemaa.',
    'by': 'V Bělorusku navštivte Minsk s moderní architekturou, Brest s pevností, a krásné národní parky jako Białowieża.',
    'ua': 'Na Ukrajině navštivte Kyjev s jeho chrámy, Lvov s kulturním dědictvím, a krásné Karpaty.',
    'md': 'V Moldavsku navštivte Kišiněv s jeho parky, historické kláštery jako Orheiul Vechi, a vinařské oblasti.',
    'al': 'V Albánii navštivte Tiranu s jejím náměstím, historické město Berat, a krásné pláže na Jónském pobřeží.',
    'mk': 'V Severní Makedonii navštivte Skopje s jeho památkami, Ohrid s jezerem, a historické město Bitola.',
    'ba': 'V Bosně a Hercegovině navštivte Sarajevo s jeho historií, Mostar s jeho mostem, a krásné hory v národním parku Sutjeska.',
    'me': 'V Černé Hoře navštivte Podgoricu s jejími památkami, Kotor s fjordy, a krásné pláže na Budvě.',
    'rs': 'V Srbsku navštivte Bělehrad s jeho pevností, Novi Sad s kulturním dědictvím, a přírodní krásy jako Tara.',
    'mt': 'Na Maltě navštivte Valletta s jejími hradbami, historické město Mdina, a krásné pláže jako Golden Bay.',
    'is': 'Na Islandu navštivte Reykjavik s moderní architekturou, Zlatý okruh s vodopády a gejzíry, a nádhernou přírodu jako Jökulsárlón.',
    'lu': 'V Lucembursku navštivte Lucemburk s jeho pevnostmi, historické město Vianden, a krásné údolí řeky Mosely.',
    'cy': 'Na Kypru navštivte Nikósii s jejími památkami, Limassol s přístavem, a krásné pláže na Pafosu.',
    'li': 'V Lichtenštejnsku navštivte Vaduz s jeho hradem, Malbun s lyžařským střediskem, a krásnou přírodu Rýnského údolí.'
};

var CityPOIButton = {
}
var CityPOI = {
    'cz': ['Karlovy Vary', 'Praha', 'České Budějovice', 'Plzeň'],
    'sk': ['Bratislava', 'Vysoké Tatry', 'Piešťany'],
    'de': ['Berlín', 'Mnichov', 'Neuschwanstein', 'Hamburk'],
    'fr': ['Paříž', 'Nice', 'Lyon', 'Bordeaux'],
    'it': ['Řím', 'Benátky', 'Florencie', 'Milán'],
    'es': ['Barcelona', 'Madrid', 'Sevilla', 'Valencie'],
    'uk': ['Londýn', 'Oxford', 'Edinburgh', 'Liverpool'],
    'at': ['Vídeň', 'Salzburg', 'Innsbruck', 'Graz'],
    'be': ['Brusel', 'Bruggy', 'Antverpy', 'Gent'],
    'nl': ['Amsterdam', 'Rotterdam', 'Haag', 'Utrecht'],
    'pl': ['Varšava', 'Krakov', 'Gdaňsk', 'Poznaň'],
    'se': ['Stockholm', 'Göteborg', 'Malmö', 'Uppsala'],
    'no': ['Oslo', 'Bergen', 'Trondheim', 'Stavanger'],
    'fi': ['Helsinky', 'Turku', 'Tampere', 'Oulu'],
    'dk': ['Kodaň', 'Aarhus', 'Odense', 'Aalborg'],
    'ch': ['Curych', 'Ženeva', 'Basilej', 'Bern'],
    'pt': ['Lisabon', 'Porto', 'Faro', 'Coimbra'],
    'gr': ['Athény', 'Soluň', 'Heraklion', 'Patras'],
    'ie': ['Dublin', 'Cork', 'Galway', 'Limerick'],
    'hu': ['Budapešť', 'Debrecín', 'Szeged', 'Miskolc'],
    'ro': ['Bukurešť', 'Cluj-Napoca', 'Temešvár', 'Iași'],
    'bg': ['Sofie', 'Plovdiv', 'Varna', 'Burgas'],
    'hr': ['Záhřeb', 'Dubrovník', 'Split', 'Rijeka'],
    'si': ['Lublaň', 'Bled', 'Maribor', 'Celje'],
    'lv': ['Riga', 'Daugavpils', 'Liepāja', 'Jelgava'],
    'lt': ['Vilnius', 'Kaunas', 'Klaipėda', 'Šiauliai'],
    'ee': ['Tallinn', 'Tartu', 'Narva', 'Pärnu'],
    'by': ['Minsk', 'Brest', 'Hrodna', 'Vitebsk'],
    'ua': ['Kyjev', 'Lvov', 'Oděsa', 'Charkov'],
    'md': ['Kišiněv', 'Tiraspol', 'Bălți', 'Bender'],
    'al': ['Tirana', 'Drač', 'Vlora', 'Shkodër'],
    'mk': ['Skopje', 'Ohrid', 'Bitola', 'Tetovo'],
    'ba': ['Sarajevo', 'Banja Luka', 'Mostar', 'Tuzla'],
    'me': ['Podgorica', 'Kotor', 'Budva', 'Herceg Novi'],
    'rs': ['Bělehrad', 'Novi Sad', 'Niš', 'Kragujevac'],
    'mt': ['Valletta', 'Sliema', 'St. Julian\'s', 'Mdina'],
    'is': ['Reykjavik', 'Akureyri', 'Keflavík', 'Selfoss'],
    'lu': ['Lucemburk', 'Esch-sur-Alzette', 'Differdange', 'Dudelange'],
    'cy': ['Nikósie', 'Limassol', 'Larnaka', 'Pafos'],
    'li': ['Vaduz', 'Schaan', 'Balzers', 'Triesen']
};

var hasVisited = JSON.parse(localStorage.getItem('hasVisited')) || {
    'cz': false,
    'sk': false,
    'de': false,
    'fr': false,
    'it': false,
    'es': false,
    'uk': false,
    'at': false,
    'be': false,
    'nl': false,
    'pl': false,
    'se': false,
    'no': false,
    'fi': false,
    'dk': false,
    'ch': false,
    'pt': false,
    'gr': false,
    'ie': false,
    'hu': false,
    'ro': false,
    'bg': false,
    'hr': false,
    'si': false,
    'lv': false,
    'lt': false,
    'ee': false,
    'by': false,
    'ua': false,
    'md': false,
    'al': false,
    'mk': false,
    'ba': false,
    'me': false,
    'rs': false,
    'mt': false,
    'is': false,
    'lu': false,
    'cy': false,
    'li': false,
    'mc': false,
    'sm': false,
    'ad': false,
    'va': false,
    'ru': false
};
var wantVisit = JSON.parse(localStorage.getItem('wantVisit')) || {
    'cz': false,
    'sk': false,
    'de': false,
    'fr': false,
    'it': false,
    'es': false,
    'uk': false,
    'at': false,
    'be': false,
    'nl': false,
    'pl': false,
    'se': false,
    'no': false,
    'fi': false,
    'dk': false,
    'ch': false,
    'pt': false,
    'gr': false,
    'ie': false,
    'hu': false,
    'ro': false,
    'bg': false,
    'hr': false,
    'si': false,
    'lv': false,
    'lt': false,
    'ee': false,
    'by': false,
    'ua': false,
    'md': false,
    'al': false,
    'mk': false,
    'ba': false,
    'me': false,
    'rs': false,
    'mt': false,
    'is': false,
    'lu': false,
    'cy': false,
    'li': false,
    'mc': false,
    'sm': false,
    'ad': false,
    'va': false,
    'ru': false
};
var visitCountriesList = []; //státy k navštívění
var visitCitiesList = []; //Města k navštívení
const countries = document.querySelectorAll('path'); // Vyber všechny státy
countries.forEach(country => {
    country.addEventListener('click', showCountryInfo);
});
/**
 * Zobrazuje informace o vybraném státu.
 * @param {Event} event - Událost kliknutí na stát.
 */

function showCountryInfo(event) {
    const countryName = event.target.id; // Získání názvu státu z atributu "id"
    console.log(`Kliknuto na stát: ${countryName}`);
    const infoElement = document.getElementById("popisek");
    let info = '';
    let status = '';

    // Pokud stát není navštíven
    if (!hasVisited[countryName]) {
        if (wantVisit[countryName]) {
            status = "Mám v plánu navštívit";
            info = `${countryInfo[countryName]},<br>${CountryPOI[countryName]}<br>${status}`;
        } else {
            status = "Nenavštívena";
            info = `${countryInfo[countryName]},<br>${CountryPOI[countryName]}<br>${status}`;
        }

        // Tlačítko "Již jsem navštívil"
        let btnVisit = document.createElement("button");
        btnVisit.innerHTML = "Již jsem navštívil";
        btnVisit.addEventListener('click', function () {
            hasVisited[countryName] = true;
            
            localStorage.setItem('hasVisited', JSON.stringify(hasVisited));
            status = "Navštívena";
            btnVisit.remove();
            if (btnWantVisit) btnWantVisit.remove();
            showCountryInfo(event); // Refresh the country info after visiting
        });

        // Tlačítko "Mám v plánu navštívit"
        let btnWantVisit = null;
        if (!wantVisit[countryName]) {
            btnWantVisit = document.createElement("button");
            btnWantVisit.innerHTML = "Mám v plánu navštívit";
            btnWantVisit.addEventListener('click', function () {
                wantVisit[countryName] = true;
                visitCountriesList.push(countryNames[countryName]); 
                localStorage.setItem('wantVisit', JSON.stringify(wantVisit));
                if (btnWantVisit) btnWantVisit.remove();
                showCountryInfo(event); // Refresh the country info after setting wantVisit
                // Zde zavoláme funkci pro zápis do souboru
                downloadButton.style.display = "block"; // Zobrazit tlačítko "Stáhnout seznam"
            });
        }

        infoElement.innerHTML = info;
        infoElement.appendChild(btnVisit);
        if (btnWantVisit) infoElement.appendChild(btnWantVisit);
    } else { // Pokud stát je navštíven
        status = "Navštívena";
        info = `${countryInfo[countryName]} ${visitedCountry[countryName]},<br>${CountryPOI[countryName]}<br>${status}`;
        infoElement.innerHTML = info;
    }

    // Vytvořit tlačítka pro města
    if (CityPOI[countryName]) {
        CityPOI[countryName].forEach(city => {
            let cityBtn = document.createElement("button");
            cityBtn.innerHTML = city;
            cityBtn.addEventListener('click', function () {
                if (!visitCitiesList.includes(city)) {
                    visitCitiesList.push(city);
                    console.log(`Přidáno město: ${city}`);
                    downloadButton.style.display = "block"; // Zobrazit tlačítko "Stáhnout seznam"
                }
            });
            infoElement.appendChild(cityBtn);
        });
    }

    // Vytvořit tlačítka pro města pomocí CityPOIButton
    if (CityPOIButton[countryName]) {
        CityPOIButton[countryName].forEach(button => {
            let btnContainer = document.createElement('div');
            btnContainer.innerHTML = button;
            infoElement.appendChild(btnContainer);
            let cityBtn = btnContainer.querySelector('button');
            cityBtn.addEventListener('click', function () {
                let cityName = cityBtn.id;
                if (!visitCitiesList.includes(cityName)) {
                    visitCitiesList.push(cityName);
                    console.log(`Přidáno město: ${cityName}`);
                    downloadButton.style.display = "block"; // Zobrazit tlačítko "Stáhnout seznam"
                }
            });
        });
    }

    // Zobrazit informace o městech, které chce uživatel navštívit
    if (wantToVisit[countryName]) {
        let wantToVisitInfo = document.createElement("div");
        wantToVisitInfo.innerHTML = `<br>${wantToVisit[countryName]}`;
        infoElement.appendChild(wantToVisitInfo);
    }
}

// Funkce pro stažení seznamu
function downloadVisitedCountriesList() {
    const combinedList = visitCountriesList.concat(visitCitiesList);
    const blob = new Blob([combinedList.join('\n')], { type: 'text/plain' });
    const url = URL.createObjectURL(blob);
    const link = document.createElement('a');
    link.href = url;
    link.download = 'visited_countries.txt';
    link.textContent = 'Stáhnout seznam';
    document.body.appendChild(link);
}

// Přidání event listeneru k tlačítku "Stáhnout seznam"
const downloadButton = document.getElementById('downloadButton');
downloadButton.style.display = "none"; // Skrýt tlačítko na začátku
downloadButton.addEventListener('click', downloadVisitedCountriesList);
 // Získání URL
 const currentUrl = window.location.href;

 const links = document.querySelectorAll('.HorniLista a');

 // Procházení každého linku
 links.forEach(link => {
   // Pokud URL odkazu odpovídá aktuální URL, ořidá třídu active
   if (link.href === currentUrl) {
     link.classList.add('active');
   }
 });